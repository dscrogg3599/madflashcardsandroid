package cs.mad.flashcards.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.FlashcardSet

/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {

    private var sets = FlashcardSet.Companion.getHardcodedFlashcardSets().toMutableList()
    private lateinit var setRecyclerView: RecyclerView
    private var adapter = FlashcardSetAdapter(sets)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*
            connect to views using findViewById
            setup views here - recyclerview, button
            don't forget to notify the adapter if the data set is changed
         */

        var buttonView = findViewById<FloatingActionButton>(R.id.addSetBUTTON)
        buttonView.setOnClickListener {
            var newset: FlashcardSet = FlashcardSet(" Set " + (sets.size + 1).toString())
            sets.add(newset)
            adapter.notifyDataSetChanged()
            setRecyclerView.scrollToPosition(sets.size - 1)
        }

        setRecyclerView = findViewById(R.id.setRecyclerView)
        setRecyclerView.layoutManager = GridLayoutManager(this, 2)
        setRecyclerView.setHasFixedSize(true)
        setRecyclerView.adapter = adapter
    }

}