package cs.mad.flashcards.activities

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.SwipeGesture
import kotlin.random.Random

class FlashcardSetDetailActivity : AppCompatActivity() {

    private var cards = Flashcard.getHardcodedFlashcards().toMutableList()
    private lateinit var cardRecyclerView : RecyclerView
    private var adapter = FlashcardAdapter(cards)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard_set_detail)
        /*
        connect to views using findViewById
        setup views here - recyclerview, button
        don't forget to notify the adapter if the data set is changed
        */
        var buttonView = findViewById<Button>(R.id.addButton)
        buttonView.setOnClickListener{
            var newcard: Flashcard = Flashcard("Term " + (cards.size + 1).toString(), "test")
            cards.add(newcard)
            adapter.notifyDataSetChanged()
            cardRecyclerView.scrollToPosition(cards.size - 1)
        }
        cardRecyclerView = findViewById(R.id.flashcardRecyclerView)
        cardRecyclerView.layoutManager = LinearLayoutManager(this)
        cardRecyclerView.setHasFixedSize(true)
        cardRecyclerView.adapter = adapter

        val swipeGesture = object : SwipeGesture(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                when(direction){
                    ItemTouchHelper.LEFT ->{
                        adapter.deleteItem(viewHolder.adapterPosition)
                    }
                }
            }
        }

        val touchHelper = ItemTouchHelper(swipeGesture)
        touchHelper.attachToRecyclerView(cardRecyclerView)
    }



}