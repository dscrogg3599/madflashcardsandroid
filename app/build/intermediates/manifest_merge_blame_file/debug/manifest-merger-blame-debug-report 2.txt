1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="cs.mad.flashcards"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="24"
8-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="30" />
9-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml
10
11    <application
11-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:5:5-20:19
12        android:allowBackup="true"
12-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:6:9-35
13        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
13-->[androidx.core:core:1.3.2] /Users/danielscroggins/.gradle/caches/transforms-3/7bf10eebbe8b81d76e8379643b6eb01e/transformed/core-1.3.2/AndroidManifest.xml:24:18-86
14        android:debuggable="true"
15        android:extractNativeLibs="false"
16        android:icon="@mipmap/ic_launcher"
16-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:7:9-43
17        android:label="@string/app_name"
17-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:8:9-41
18        android:roundIcon="@mipmap/ic_launcher_round"
18-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:9:9-54
19        android:supportsRtl="true"
19-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:10:9-35
20        android:testOnly="true"
21        android:theme="@style/Theme.Flashcards" >
21-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:11:9-48
22        <activity android:name="cs.mad.flashcards.activities.MainActivity" >
22-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:12:9-18:20
22-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:12:19-58
23            <intent-filter>
23-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:13:13-17:29
24                <action android:name="android.intent.action.MAIN" />
24-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:14:17-69
24-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:14:25-66
25
26                <category android:name="android.intent.category.LAUNCHER" />
26-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:16:17-77
26-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:16:27-74
27            </intent-filter>
28        </activity>
29        <activity android:name="cs.mad.flashcards.activities.FlashcardSetDetailActivity" />
29-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:19:9-84
29-->/Users/danielscroggins/Desktop/lab3-skeleton-android/app/src/main/AndroidManifest.xml:19:19-72
30    </application>
31
32</manifest>
